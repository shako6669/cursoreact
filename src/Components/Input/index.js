//@flow
import React, { Fragment, Component, useState } from "react"
import Styled from "styled-components"

export type PropsInput = {
  label?: string,
  error?: String,
  tipo: string,
  pass: true,
  name: string,
  header: any
}

const StyledInput = Styled.input`
    border: 1px grey solid;
    border-radius: 3px;
    display: block;
    outline:none ;
`
const StyledInputLabel = Styled.label`
    size: 10px;
    display: ;
`

const StyledInputError = Styled.label`
    font-size: 12px;
    font-family: arial;
    display: block;
    color: red;
`

const StyledButton = Styled.button`
  margin-left: 60px;
`

const Input = ({ label, error, tipo, pass, header: Header }: PropsInput) => {
  const [mostrar, setMostrar] = useState(false)
  const Label = label
  const Error = error
  tipo = mostrar ? "text" : "password"
  return (
    <div>
      {Header && Header}
      {Label ? <StyledInputLabel>{Label}</StyledInputLabel> : ""}
      {pass ? (
        <StyledButton onClick={() => setMostrar(!mostrar)}>
          {mostrar ? "Hide" : "Show"}
        </StyledButton>
      ) : (
        ""
      )}
      <StyledInput type={tipo} />
      {Error ? <StyledInputError>{Error}</StyledInputError> : ""}
    </div>
  )
}

Input.defaultProps = {}

export default Input
