//@flow
import React from "react"
import Input from "." //con "." por defecto es ./index

export default {
  title: "Input",
  component: Input
}

export const defaultInput = () => <Input tipo="password" />

export const inputWithLabel = () => <Input label="Email" />

export const inputWithError = () => (
  <Input error="Favor digitar email" label="Email" />
)

export const inputWithPass = () => <Input label="Password" pass />
