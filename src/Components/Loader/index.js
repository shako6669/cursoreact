// @flow
import React from "react"
import Loader from "react-loader-spinner"

const MiLoader = () => (
  <Loader
    type="RevolvingDot"
    color="blue"
    height={100}
    width={100}
    //timeout={3000} //3 secs
  />
)

export default MiLoader
