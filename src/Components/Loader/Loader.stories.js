//@flow
import React from "react"

import MiLoader from "."

export default {
  title: "MiLoader",
  component: MiLoader,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const DefaultLoader = () => <MiLoader />
