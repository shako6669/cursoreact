//@flow
import React from "react"
import Button from "." //con "." por defecto es ./index

export default {
  title: "Button",
  component: Button
}

export const defaultButton = () => <Button>Click aquí</Button>

export const mediumButton = () => <Button className="medium">Click aquí</Button>

export const underlineButton = () => (
  <Button className="medium" underline>
    Click aquí
  </Button>
)
