// @flow
import React, { useState } from "react"
import Input from "../Input"
import Styled from "styled-components"
import type { PropsInput } from "../Input"

type Props = {
  label?: string,
  showPassword?: boolean
} & PropsInput

const styledHeader = Styled.div`
    display: flex;
    justify-content: space-between;
`

const PasswordField = ({ showPassword, label, name, error, tipo }: Props) => {
  const [showPwd, setShowPwd] = useState(false)
  let inputType = showPwd ? "text" : "password"

  const Header = () => (
    <styledHeader>
      {label && <label htmlFor={name}>{label}</label>}
      {showPassword && (
        <button onClick={() => setShowPwd(!showPwd)}>
          {showPwd ? "Ocultar" : "Mostrar"}
        </button>
      )}
    </styledHeader>
  )
  return (
    <Input name={name} error={error} type={inputType} header={<Header />} />
  )
}

PasswordField.defaultProps = {
  showPassword: false
}

export default PasswordField
