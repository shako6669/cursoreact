//@flow
import React from "react"

import PasswordField from "."

export default {
  title: "PasswordField",
  component: PasswordField,
  parameters: {
    info: {
      inline: true
    }
  }
}

export const defaultPasswordField = () => <PasswordField name="password" />

export const PasswordFieldWithLabel = () => (
  <PasswordField name="password" label="Contraseña" />
)
export const PasswordFieldWithButton = () => (
  <PasswordField name="password" label="Contraseña" showPassword />
)

export const PasswordFieldWithError = () => (
  <PasswordField
    name="password"
    label="Contraseña"
    showPassword
    error="Requerido"
  />
)
