//@flow

import React from "react"
import logo from "./logo.svg"
import Styled from "styled-components"
import Input from "./Components/Input/index"

const Wrapper = Styled.div`
  text-align: center;
`

type Props = {
  label?: string | any, //el signo de interrogacion al final del parametro indica que puede ser opcional
  //el signo | indica que puede ser un tipo u otro tipo
  to: string,
  children?: any
} //& Otro tipo de propiedad

const Link = ({ label, to, children }: Props) => (
  <a className="App-link" href={to} target="_blank" rel="noopener noreferrer">
    {label || children}
  </a>
)
//label = Learn React
//to = "https://reactjs.org"

function App() {
  return (
    <Wrapper className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Link to="https://reactjs.org" label="Learn React"></Link>
        <Input />
      </header>
    </Wrapper>
  )
}
//Eliminar los punto y coma con prettier
export default App
