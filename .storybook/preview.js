import React from "react"

import { addDecorator } from "@storybook/react"
import { withInfo } from "@storybook/addon-info"
import { ThemeProvider } from "styled-components"
import theme from "../src/config/theme"
import GlobalStyles from "../src/config/GlobalStyles"

addDecorator(withInfo)

addDecorator(story => (
  <ThemeProvider theme={theme}>
    {story()}
    <GlobalStyles />
  </ThemeProvider>
))
